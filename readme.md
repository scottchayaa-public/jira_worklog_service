# JIRA Worklog Service

## ENV

```bash
cp .env.example .env
```

Update .env for your Jira setting.

## Install & Run
```bash
yarn install

# run service
node index.js
```

## API List
http://localhost:3000

