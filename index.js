require('dotenv').config();
const moment = require('moment');
const express = require('express');

const app = express();
const port = process.env.PORT;

const JIRA_ACCOUNT = process.env.JIRA_ACCOUNT;
const JIRA_TOKEN = process.env.JIRA_TOKEN;
const JIRA_ACCOUNT_ID = process.env.JIRA_ACCOUNT_ID;

const JiraService = require('./JiraService');
var jiraService = new JiraService(JIRA_ACCOUNT, JIRA_TOKEN);

////////////////////////////////////////////////////////////

app.listen(port, () => {
    console.log(`Pandda Service listening on port : ${port} ...`);
});
  
app.get('/', async (req, res) => {
    var start_date = moment().add(-1, 'days').format('YYYY-MM-DD');
    var end_date = moment().format('YYYY-MM-DD');

    res.send(await jiraService.getSearch(start_date, end_date, JIRA_ACCOUNT_ID));
});

