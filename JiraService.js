const fetch = require('node-fetch');

class JiraService {
    _APIHOST = 'https://coolbee.atlassian.net/rest/api/3';
    _account = '';
    _token = '';
    _Authorization = '';

    constructor(account, token) {
        this._account = account;
        this._token = token;
        this._Authorization = `Basic ${Buffer.from(`${account}:${token}`).toString('base64')}`;
    }

    getWorkLogs = async (timestamp) =>
    {
        try {
            var response = await fetch(`${this._APIHOST}/worklog/updated?since=${timestamp}`, {
                method: 'GET',
                headers: {
                    'Authorization': this._Authorization,
                    'Accept': 'application/json'
                }
            });

            console.log( `Response: ${response.status} ${response.statusText}` );
            
            return JSON.parse(await response.text());
        } catch (error) {
            console.error(error)
        }
    }

    getSearch = async (start_date, end_date, account_id) =>
    {
        var jql = `worklogDate>='${start_date}' AND worklogDate<='${end_date}' AND worklogAuthor=${account_id}`;
        var fields = `summary,parent,key,issuetype,worklog`; // customfield_10014 看起來不需要

        console.log(`jql = ${jql}`);

        try {
            var response = await fetch(`${this._APIHOST}/search?jql=${jql}&fields=${fields}`, {
                method: 'GET',
                headers: {
                    'Authorization': this._Authorization,
                    'Accept': 'application/json'
                }
            });

            console.log( `Response: ${response.status} ${response.statusText}` );
            
            return JSON.parse(await response.text());
        } catch (error) {
            console.error(error)
        }
    }
}

module.exports = JiraService;